NODE 18, NPM & DEV TOOLS (angular,ionic,nestjs,nodemon)

```
curl -sL https://gitlab.com/ep-public/scripts/raw/master/nodejs/node18.sh | sudo -E bash -
```


NODE 14, NPM & PM2

```
curl -sL https://gitlab.com/ep-public/scripts/raw/master/nodejs/node14.sh | sudo -E bash -
```

NODE *, NPM & PM2

```
curl -sL https://gitlab.com/ep-public/scripts/raw/master/nodejs/node.sh | sudo -E bash -s 12
curl -sL https://gitlab.com/ep-public/scripts/raw/master/nodejs/node.sh | sudo -E bash -s 11
curl -sL https://gitlab.com/ep-public/scripts/raw/master/nodejs/node.sh | sudo -E bash -s 10
curl -sL https://gitlab.com/ep-public/scripts/raw/master/nodejs/node.sh | sudo -E bash -s 9
curl -sL https://gitlab.com/ep-public/scripts/raw/master/nodejs/node.sh | sudo -E bash -s 8
```
