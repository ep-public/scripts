if [[ -z "$6" ]]; then

tput setaf 11
echo -e "\nBEFORE (from device):"
tput setaf 4
echo -e "auth target device to ssh server using ssh-keygen (no needs replace key if exists) and ssh-copy-id"
tput setaf 11
echo -e "\nSCRIPT PARAMS:" 
tput setaf 5
echo -e "Param 1 :> Service Name (without spaces and not too long)"
echo -e "Param 2 :> Reverse Proxy Mapping --> {remote port}"
echo -e "Param 3 :> Reverse Proxy Mapping --> {local ip or hostname}:{local port}"
echo -e "Param 4 :> SSH Server Host Name or IP"
echo -e "Param 5 :> Local User"
echo -e "Param 6 :> SSH Server User"
echo -e "curl -sL https://gitlab.com/ep-public/scripts/raw/master/ssh-tunnel/setup.sh | sudo -E bash -s {param1} {param2} {param3} {param4} {param5} {param6}"
tput setaf 11
echo -e "\nSAMPLE (setup from device):"
tput setaf 5
echo -e "curl -sL https://gitlab.com/ep-public/scripts/raw/master/ssh-tunnel/setup.sh | sudo -E bash -s router-webui 23080 192.168.1.1:80 service.eagleprojects.cloud pi administrator"
tput setaf 11
echo -e "SAMPLE (use from anywhere):"
tput setaf 5
echo -e "http://service.eagleprojects.cloud:23080"
tput setaf 7 
  
elif [[ -n "$6" ]]; then

tput setaf 3
echo -e ">>> SETUP SYSTEMD (AUTO-START)\n"
tput setaf 7 

sudo echo "[Unit]
Description=SSH Tunnel Service
After=network.target
[Service]
User=$5
ExecStart=/usr/bin/ssh -NT -o ExitOnForwardFailure=yes -o ServerAliveInterval=60 -o ServerAliveCountMax=3 -R $2:$3 -i /home/$5/.ssh/id_rsa $6@$4
RestartSec=5
Restart=always
[Install]
WantedBy=multi-user.target" > /etc/systemd/system/ep-$1.service

sudo systemctl enable ep-$1
sudo systemctl start ep-$1

tput setaf 11
echo -e "\nDONE !\n" 
tput setaf 5
echo -e "Service Name           :> ep-$1"
echo -e "Service File           :> /etc/systemd/system/ep-$1.service"
echo -e "SSH Server             :> $4"
echo -e "Reverse Proxy Mapping  :> (local) $3 -> $4:$2 (from anywhere)"
echo -e "Local User             :> $5"
echo -e "SSH Server User        :> $6"
tput setaf 11
echo -e "\nUSE (samples)\n" 
tput setaf 5
echo -e "(browser) http://$4:$2"
echo -e "(bash) ssh -p $2 $5@$4"
tput setaf 7 

fi
