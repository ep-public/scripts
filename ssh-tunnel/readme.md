### HowTo

Permette di connettersi via ssh a device linux passando tramite un server.
Le device sono sempre client ssh del server in modalità reverse-proxy
Lo smistamento avviene tramite la porta del server.

Adatto per amministraione della device non per traffico costante sostanzioso poichè passerebbe dal server

{server-name} ad esempio: server.nome-dominio.com

**Setup Server**
- Open Port (Range) FATTO (su OVH le porte sono tutte aperte di default, su Azure invece no)

**Setup Device**

**STEP 1** - Creazione chiave e trasferimento chiave (accreditamento della device al server)
eseguire:
```
ssh-keygen
ssh-copy-id {server-name}
```

ad esempio:
```
ssh-keygen
ssh-copy-id server.nome-dominio.com
```

**STEP 2** - Installazione del servizio che manterrà il tunnel acceso

```sh 
curl -sL https://gitlab.com/ep-public/scripts/raw/master/ssh-tunnel/setup.sh | sudo -E bash -s {user@server} {port} {local-user}
```
ad esempio:
```
curl -sL https://gitlab.com/ep-public/scripts/raw/master/ssh-tunnel/setup.sh | sudo -E bash -s srv-user@server.nome-dominio.com 21345 device-user
```

**Connect from PC**
```sh
ssh -p 21345 device-user@server.nome-dominio.com
```