#!/bin/bash
GIT_TOKEN="$1"
ID="$2"
GROUP="$3"
ORG="$4"
BUCKET="$5"
INFLUX_TOKEN="$6"
INFLUX_URL="$7"

if [ "$GIT_TOKEN" == "" ]
then
    echo "Parametro GIT_TOKEN non passato"
    exit 1;
fi

if [ "$ID" == "" ]
then
    echo "Parametro UID non passato"
    exit 1;
fi

if [ "$GROUP" == "" ]
then
    echo "Parametro GROUP non passato"
    exit 1;
fi

if [ "$ORG" == "" ]
then
    echo "Parametro ORG non passato"
    exit 1;
fi

if [ "$BUCKET" == "" ]
then
    echo "Parametro BUCKET non passato"
    exit 1;
fi

if [ "$INFLUX_TOKEN" == "" ]
then
    echo "Parametro INFLUX_TOKEN non passato"
    exit 1;
fi

sudo tput setaf 3
sudo echo -e "\n(1/5) >>> SETUP SYSTEM\n"
sudo tput setaf 7

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common git jq

curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo npm i -g npm
sudo npm i -g typescript pm2

sudo tput setaf 3
sudo echo -e "\n(2/5) >>> DOWNLOAD PROJECT\n"
sudo tput setaf 7

sudo git clone -b main https://gitlab+deploy-token-558854:$GIT_TOKEN@gitlab.com/ep-servers/ep-health-metrics.git
cd ep-health-metrics

sudo tput setaf 3
sudo echo -e "\n(3/5) >>> DOWNLOAD DEPENDENCIES\n"
sudo tput setaf 7

sudo npm i

sudo tput setaf 3
sudo echo -e "\n(4/5) >>> COMPILE PROJECT\n"
sudo tput setaf 7

sudo tsc

sudo chown -R $USER .
cat default_config.json | jq ".uid = \"$ID\" | .group = \"$GROUP\" | .influx_org = \"$ORG\" | .influx_bucket = \"$BUCKET\" | .influx_token = \"$INFLUX_TOKEN\"" > ./config.json

if [ "$INFLUX_URL" != "" ]
then
    echo "OVERRIDE INFLUX URL WITH $INFLUX_URL"
    old="`cat ./config.json`"
    echo $old | jq ".influx_url = \"$INFLUX_URL\"" > ./config.json
fi

sudo tput setaf 3
sudo echo -e "\n(5/5) >>> SETUP PM2\n"
sudo tput setaf 7

sudo pm2 start build/main.js --name ep-health-metrics
sudo tput setab 0 # riporto il bg a nero

sudo pm2 startup
sudo tput setab 0 # riporto il bg a nero

sudo pm2 save
sudo tput setab 0 # riporto il bg a nero

sudo tput setaf 5
sudo echo -e "\nDONE" 
sudo tput setaf 7

