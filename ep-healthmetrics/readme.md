### Prepare

- use **install** script first time
- use **update** script when project is updated also with npm packages
- use **update only prj** when only prj is updated (package.json unchanged) 

### Install
```sh 
curl -sL https://gitlab.com/ep-public/scripts/raw/master/ep-healthmetrics/install.sh | sudo -E bash -s {GITLAB_TOKEN} {DEVICE-ID} {DEVICE-GROUP} {INFLUX-ORG} {INFLUX-BUCKET} {INFLUX-TOKEN} 
```

### Update
```sh 
curl -sL https://gitlab.com/ep-public/scripts/raw/master/ep-healthmetrics/update.sh | sudo -E bash -
```

### Update Only Project (no dependencies changed)
```sh 
curl -sL https://gitlab.com/ep-public/scripts/raw/master/ep-healthmetrics/update.only.prj.sh | sudo -E bash -
```
